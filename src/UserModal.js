import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import EmailIcon from '@material-ui/icons/Email';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import PersonIcon from '@material-ui/icons/Person';
import ContactsIcon from '@material-ui/icons/Contacts';
import ScheduleIcon from '@material-ui/icons/Schedule';
import axios from 'axios';
import moment from 'moment';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs(props) {
  const [open, setOpen] = React.useState(false);
  const [data,setData] = React.useState({
      name:"",
      email:"",
      contactNumber:"",
      created:""
  });

  const handleClickOpen = async() => {
    const url ='/admin/finduser/'+props.userid;
    let userdata=await axios.get(url);
    console.log(userdata.data)
    setData(userdata.data);
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        {props.userid}
      </Button>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
         {data.name}
        </DialogTitle>
        <DialogContent dividers>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
              <TableBody>
                  <TableRow>
                      <TableCell><PersonIcon/></TableCell>
                      <TableCell>{data.name}</TableCell>
                  </TableRow>
                  <TableRow>
                      <TableCell><EmailIcon/></TableCell>
                      <TableCell>{data.email}</TableCell>
                  </TableRow>
                  <TableRow>
                      <TableCell><ContactsIcon/></TableCell>
                      <TableCell>{data.contactNumber}</TableCell>
                  </TableRow>
                  <TableRow>
                      <TableCell><ScheduleIcon/></TableCell>
                      <TableCell>{Date(data.created)}</TableCell>
                  </TableRow>
              </TableBody>
          </Table>
        </TableContainer>  
        </DialogContent>
      </Dialog>
    </div>
  );
}
