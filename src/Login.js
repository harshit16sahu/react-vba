import React, { Component } from 'react';
import './css/style.css';
import { BrowserRouter as Router, Link,useHistory } from 'react-router-dom';
import './css/materialize.css';
import './css/mob.css';
import './css/bootstrap.css';
import Axios from 'axios';

class Login extends Component{
  constructor(props){
    super(props)
    if (localStorage.admintoken) {
			this.props.history.push(`/`);
		}
    this.state={
      email:'',
      password:'',
      errors:[]
    }
  }
  
  login=(e)=>{
    e.preventDefault();
// ---------------------------------
// this.props.history.push(`/`);	
// --------------------------------
		try {
			Axios
				.post("admin/login", {
					email: this.state.email,
					password: this.state.password
				})
				.then(response => {
					console.log("ress - " + response.message);

					if (response.data === "Error: Wrong Email/Password") {
						console.log("Login failed");
						let arr = [];
						arr.push(response.data);

						this.setState({
							errors: arr
						});
					} else if (response.data) {
            console.log(response.data.message);
						localStorage.setItem("admintoken", response.data.token);
						this.props.history.push(`/`);
					}
				})
				.catch(err => {
					console.log(err);
				});
		} catch (e) {
			console.log("Error in Login");
			console.log(e);
		}
	};
    
  
  handlepass=(e)=>{
      this.setState({password:e.target.value})
  }
  handleemail=(e)=>{
    this.setState({email:e.target.value})
}
  render() {
    
    
    return (
      
      
      <div>
               
        <div className="blog-login">
          <div className="blog-login-in">
            <form>
              <img src="images/logo.png" alt="" />
              <div className="row">
                <div className="input-field col s12">
                  <input id="first_name1" type="text" className="validate" value={this.state.email} onChange={this.handleemail}/>
                  <label htmlFor="first_name1">User Name</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s12">
                  <input id="last_name" type="password" className="validate" value={this.state.password} onChange={this.handlepass} />
                  <label htmlFor="last_name">Password</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s12">
                  <a className="waves-effect waves-light btn-large btn-log-in" onClick={this.login}>Login</a>
                </div>
              </div>
              <Link to="Forgot" className="for-pass">Forgot Password?</Link>
            </form>
          </div>
        </div>
        {/*======== SCRIPT FILES =========*/}
      </div>
    );
  }
}

export default Login