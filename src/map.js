import { Route, Switch, Redirect } from 'react-router-dom';
import React from 'react';
import {connect} from 'react-redux'
import Login from './Login';
import App from './App';
class Main extends React.Component{
    constructor(props){
        super(props);
        this.state={
            loggedin:false
        }
    }
    componentDidMount(){
        if (localStorage.usertoken) {
            
            this.setState({loggedin:true})
            console.log(this.state.loggedin);
        }
        
    }
    render(){
        return(
            <>
                {this.state.loggedin ? <App />:<Redirect to='/Login' />}
            </>
        )
    }
}

export default Main;
