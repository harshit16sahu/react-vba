import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
//import '../node_modules/font-awesome/css/font-awesome.min.css';
import './css/font-awesome.min.css';
import { BrowserRouter ,Switch,Route} from 'react-router-dom';
import Login from './LoginPrad';
import Home from './Home'
import ListingAll from './ListingAll'



ReactDOM.render(
  <BrowserRouter>
  <Switch>
      <Route exact path='/Login' component={Login}/>
      <Route exact path='/' component={Home}/>
      <Route exact path='/ListingAll' component={ListingAll}/>
  </Switch>
  <App/>
  </BrowserRouter>,
  document.getElementById('root')
  );


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
